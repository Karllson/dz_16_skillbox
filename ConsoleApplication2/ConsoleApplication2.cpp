﻿#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime>
using namespace std;

// Создание матрицы

int** Add(int n, int m)
{
    int** M = new int* [n];
    for (int i = 0; i < n; ++i)
    {
        M[i] = new int[m];
    }
    return M;
}

// Удаление матрицы

void Delate(int** M, int n)
{
    for (int i = 0; i < n; ++i)
    {
        delete[] M[i];
    }
}

//Заполнение матрицы

void Input(int** M, int n, int m)
{
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < m; ++j)
        {
            M[i][j] = i + j;
        }
    }

}

//Печать матрицы 

void Print(int** M, int n, int m)
{
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < m; ++j)
        {
            cout << M[i][j] << ' ';
        }
        cout << std::endl;
    }
}

//  Сумма элеменитов строки

void Summ(int** M, int n, int m)
{
    time_t t;
    time(&t);
    int d = (localtime(&t)->tm_mday) % n;

    for (int i = 0; i < n; ++i)
    {
        int s = 0;

        for (int j = 0; j < n; ++j)
            s += M[d][j];

        cout << "Сумма элементов строки " << d << " = " << s;

        break;
    }
}


int main()
{
    setlocale(LC_ALL, "rus");
    int n;

    cout << "Введите N:";
    cin >> n;

    int** A = Add(n, n);

    Input(A, n, n);

    Print(A, n, n);

    Summ(A, n, n);
    
    Delate(A, n);

    return 0;
}
